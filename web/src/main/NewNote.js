// Copyright 2021 The casbin Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import * as Setting from "../Setting";
import * as NoteBackend from "../backend/NoteBackend";
import i18next from "i18next";
import { withRouter, Link } from "react-router-dom";
import Select2 from "react-select2-wrapper";

class NewNote extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: props,
      form: {
        parent: "/",
      },
      message: "",
      folders: [],
      field: "file",
    };
    if (this.props.location.query?.field != undefined) {
      this.state.field = this.props.location.query?.field;
    }
    this.state.url = `/notes/new`;
  }

  componentDidMount() {
    this.getFolder();
  }

  getFolder() {
    NoteBackend.getNotesByParent("/").then((res) => {
      let folders = [];
      for (let i in res) {
        if (res[i].Field == "folder") {
          folders.push(res[i]);
        }
      }
      folders.push({ Name: "/" });
      this.setState({
        folders: folders,
      });
    });
  }

  inputChange(event, id) {
    let form = this.state.form;
    form[id] = event.target.value;
    this.setState({
      form: form,
    });
  }

  textInput(event, id) {
    event.target.style.height = event.target.scrollHeight + "px";
    let form = this.state.form;
    form[id] = event.target.value;
    this.setState({
      form: form,
    });
  }

  clearMessage() {
    this.setState({
      message: "",
    });
  }

  addNote() {
    let form = this.state.form;
    form["field"] = this.state.field;
    NoteBackend.addNote(form).then((res) => {
      if (res.data == true) {
        this.setState({
          message: i18next.t("new:File or folder added successfully"),
        });
      } else {
        this.setState({
          message: res?.msg,
        });
      }
    });
  }

  render() {
    return (
      <div>
        <div className="box">
          <div className="header">
            <Link to="/">{Setting.getForumName()}</Link>{" "}
            <span className="chevron">&nbsp;›&nbsp;</span>{" "}
            <Link to="/notes">{i18next.t("general:Note")}</Link>
            <span className="chevron">&nbsp;›&nbsp;</span>
            {i18next.t("note:Create")}
          </div>
        </div>
        <div className="box">
          {this.state.message !== "" ? (
            <div className="message" onClick={() => this.clearMessage()}>
              <li className="fa fa-exclamation-triangle"></li>
              &nbsp; {this.state.message}
            </div>
          ) : null}
          <div className="inner">
            <table cellPadding="5" cellSpacing="0" border="0" width="100%">
              <tbody>
                <tr>
                  <td width={Setting.PcBrowser ? "120" : "90"} align="right">
                    {this.state.field == "folder"
                      ? i18next.t("note:Folder Name")
                      : i18next.t("note:File Name")}
                  </td>
                  <td width="auto" align="left">
                    <input
                      onChange={(event) => this.inputChange(event, "name")}
                    />
                  </td>
                </tr>
                {this.state.field == "file" ? (
                  <tr>
                    <td width={Setting.PcBrowser ? "120" : "90"} align="right">
                      {i18next.t("note:Parent Folder")}
                    </td>
                    <td width="auto" align="left">
                      <Select2
                        value={this.state.form.parent}
                        style={{ width: "110px", fontSize: "14px" }}
                        data={this.state.folders.map((folder, i) => {
                          return { text: `${folder.Name}`, id: i };
                        })}
                        onSelect={(event) => {
                          const s = event.target.value;
                          if (s == "") {
                            return;
                          }
                          const index = parseInt(s);
                          let form = this.state.form;
                          form["parent"] = this.state.folders[index]?.Name;
                          this.setState({
                            form: form,
                          });
                        }}
                        options={{ placeholder: this.state.form.parent }}
                      />
                    </td>
                  </tr>
                ) : null}
                {this.state.field == "file" ? (
                  <tr>
                    <td width={Setting.PcBrowser ? "120" : "90"} align="right">
                      {i18next.t("note:File Content")}
                    </td>
                    <td width="auto" align="left">
                      <textarea
                        rows="5"
                        style={{ width: "60%" }}
                        value={this.state.form["content"]}
                        onChange={(event) => this.textInput(event, "content")}
                      />
                    </td>
                  </tr>
                ) : null}
                <tr>
                  <td
                    width={Setting.PcBrowser ? "120" : "90"}
                    align="right"
                  ></td>
                  <td width="auto" align="left">
                    <input
                      type="submit"
                      className="super normal button"
                      value={i18next.t("note:Save")}
                      onClick={() => this.addNote()}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(NewNote);
